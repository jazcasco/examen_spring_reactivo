package com.mitocode.examen.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.examen.document.Estudiante;
import com.mitocode.examen.repo.IEstudianteRepo;
import com.mitocode.examen.service.IEstudianteService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author jcasco
 */
@Service
public class EstudianteServiceImpl implements IEstudianteService {

	@Autowired
	private IEstudianteRepo repo;

	@Override
	public Mono<Estudiante> registrar(Estudiante t) {
		return repo.save(t);
	}

	@Override
	public Mono<Estudiante> modificar(Estudiante t) {
		return repo.save(t);
	}

	@Override
	public Flux<Estudiante> listar() {
		return repo.findAll();
	}

	@Override
	public Mono<Estudiante> listarPorId(String v) {
		return repo.findById(v);
	}

	@Override
	public Mono<Void> eliminar(String v) {
		return repo.deleteById(v);
	}
}
