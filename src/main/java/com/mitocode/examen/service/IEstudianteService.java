package com.mitocode.examen.service;

import com.mitocode.examen.document.Estudiante;

/**
 * @author jcasco
 */
public interface IEstudianteService extends ICRUD<Estudiante, String> {

}
