package com.mitocode.examen.service;


import com.mitocode.examen.document.Usuario;
import com.mitocode.examen.security.User;

import reactor.core.publisher.Mono;

/**
 * @author jcasco
 */
public interface IUsuarioService extends ICRUD<Usuario, String> {
	Mono<User> buscarPorUsuario(String usuario);

}
