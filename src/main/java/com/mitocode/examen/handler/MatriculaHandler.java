package com.mitocode.examen.handler;

import static org.springframework.web.reactive.function.BodyInserters.fromValue;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.mitocode.examen.document.Matricula;
import com.mitocode.examen.service.IMatriculaService;
import com.mitocode.examen.validator.RequestValidator;

import reactor.core.publisher.Mono;

/**
 * @author jcasco
 */
@Component
public class MatriculaHandler {

	@Autowired
	private IMatriculaService service;

	@Autowired
	private RequestValidator validadorGeneral;

	public Mono<ServerResponse> registrar(ServerRequest req) {
		Mono<Matricula> mono = req.bodyToMono(Matricula.class);

		return mono.flatMap(this.validadorGeneral::validar)
				.flatMap(service::registrar)
				.flatMap(p -> ServerResponse.created(URI.create(req.uri().toString().concat("/").concat(p.getId())))
						.contentType(MediaType.APPLICATION_STREAM_JSON).body(fromValue(p)));

	}

}
