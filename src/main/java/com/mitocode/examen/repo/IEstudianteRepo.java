package com.mitocode.examen.repo;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.mitocode.examen.document.Estudiante;

/**
@author jcasco
*/
public interface IEstudianteRepo extends ReactiveMongoRepository<Estudiante, String>{

}
