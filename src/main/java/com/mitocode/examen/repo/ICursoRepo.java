package com.mitocode.examen.repo;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.mitocode.examen.document.Curso;

/**
 * @author jcasco
 */
public interface ICursoRepo extends ReactiveMongoRepository<Curso, String> {

}
