package com.mitocode.examen.repo;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.mitocode.examen.document.Matricula;

/**
 * @author jcasco
 */
public interface IMatriculaRepo extends ReactiveMongoRepository<Matricula, String> {

}
