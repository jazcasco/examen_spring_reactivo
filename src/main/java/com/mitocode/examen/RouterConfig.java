package com.mitocode.examen;

import static org.springframework.web.reactive.function.server.RequestPredicates.DELETE;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RequestPredicates.PUT;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.mitocode.examen.handler.CursoHandler;
import com.mitocode.examen.handler.EstudianteHandler;
import com.mitocode.examen.handler.MatriculaHandler;

@Configuration
public class RouterConfig {
		
	@Bean
	public RouterFunction<ServerResponse> rutasCursos(CursoHandler handler){
		return route(GET("/v2/cursos"), handler::listar)
				.andRoute(GET("/v2/cursos/{id}"), handler::listarPorId)
				.andRoute(POST("/v2/cursos"), handler::registrar)
				.andRoute(PUT("/v2/cursos"), handler::modificar)
				.andRoute(DELETE("/v2/cursos/{id}"), handler::eliminar);	
	}
	
	@Bean
	public RouterFunction<ServerResponse> rutasEstudiantes(EstudianteHandler handler){
		return route(GET("/v2/estudiante"), handler::listar)
				.andRoute(GET("/v2/estudiante/{id}"), handler::listarPorId)
				.andRoute(POST("/v2/estudiante"), handler::registrar)
				.andRoute(PUT("/v2/estudiante"), handler::modificar)
				.andRoute(DELETE("/v2/estudiante/{id}"), handler::eliminar);	
	}
	
	@Bean
	public RouterFunction<ServerResponse> rutasMatriculas(MatriculaHandler handler){
		return route(POST("/v2/matricula"), handler::registrar);	
	}
}
