package com.mitocode.examen.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.examen.document.Matricula;
import com.mitocode.examen.service.IMatriculaService;

import reactor.core.publisher.Mono;

/**
 * @author jcasco
 */
@RestController
@RequestMapping("/matricula")
public class MatriculaController {

	@Autowired
	private IMatriculaService service;

	@PostMapping
	public Mono<ResponseEntity<Matricula>> registrar(@Valid @RequestBody Matricula matricula,
			final ServerHttpRequest req) {
		return service.registrar(matricula)
				.map(p -> ResponseEntity.created(URI.create(req.getURI().toString().concat("/").concat(p.getId())))
						.contentType(MediaType.APPLICATION_STREAM_JSON).body(p));
	}
}
