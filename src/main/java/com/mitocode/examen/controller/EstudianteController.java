package com.mitocode.examen.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.examen.document.Estudiante;
import com.mitocode.examen.service.IEstudianteService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

/**
 * @author jcasco
 */

@RestController
@RequestMapping("/estudiante")
public class EstudianteController {

	@Autowired
	private IEstudianteService service;

	@GetMapping
	public Mono<ResponseEntity<Flux<Estudiante>>> listar() {
		Flux<Estudiante> estudiantesFlux = service.listar();

		return Mono.just(ResponseEntity.ok().contentType(MediaType.APPLICATION_STREAM_JSON).body(estudiantesFlux));
	}

	@GetMapping("/{id}")
	public Mono<ResponseEntity<Estudiante>> listarPorId(@PathVariable("id") String id) {
		return service.listarPorId(id) // Mono.empty()
				// .defaultIfEmpty(new Estudiante())
				.map(p -> ResponseEntity.ok().contentType(MediaType.APPLICATION_STREAM_JSON).body(p))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}

	@PostMapping
	public Mono<ResponseEntity<Estudiante>> registrar(@Valid @RequestBody Estudiante estudiante,
			final ServerHttpRequest req) {
		return service.registrar(estudiante)
				.map(p -> ResponseEntity.created(URI.create(req.getURI().toString().concat("/").concat(p.getId())))
						.contentType(MediaType.APPLICATION_STREAM_JSON).body(p))
				.onErrorMap(error -> new ArithmeticException("Ups"));
	}

	@PutMapping
	public Mono<ResponseEntity<Estudiante>> modificar(@Valid @RequestBody Estudiante estudiante) {
		return service.modificar(estudiante)
				.map(p -> ResponseEntity.ok().contentType(MediaType.APPLICATION_STREAM_JSON).body(p))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}

	@DeleteMapping("/{id}")
	public Mono<ResponseEntity<Void>> eliminar(@PathVariable("id") String id) {
		return service.listarPorId(id).flatMap(p -> {
			return service.eliminar(p.getId()).then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
		}).defaultIfEmpty(ResponseEntity.notFound().build());
	}

	@GetMapping("/listar")
	public Flux<Estudiante> listarDescPorEdad() {
		return service.listar().flatMap(p -> service.listarPorId(p.getId()))
				.sort((p1, p2) -> p2.getEdad().intValue() - p1.getEdad().intValue());
	}

	@GetMapping("/listar/parallel")
	public Flux<Estudiante> listarDescPorEdadParallel() {

		return service.listar().parallel().runOn(Schedulers.elastic()).flatMap(p -> service.listarPorId(p.getId()))
				.ordered((p1, p2) -> p2.getEdad().intValue() - p1.getEdad().intValue());
	}
}
