package com.mitocode.examen.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.examen.document.Curso;
import com.mitocode.examen.service.ICursoService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/curso")
public class CursoController {

	@Autowired
	private ICursoService service;

	@GetMapping
	public Mono<ResponseEntity<Flux<Curso>>> listar() {
		Flux<Curso> platosFlux = service.listar();

		return Mono.just(ResponseEntity.ok().contentType(MediaType.APPLICATION_STREAM_JSON).body(platosFlux));
	}

	@GetMapping("/{id}")
	public Mono<ResponseEntity<Curso>> listarPorId(@PathVariable("id") String id) {
		return service.listarPorId(id) // Mono.empty()
				// .defaultIfEmpty(new Curso())
				.map(p -> ResponseEntity.ok().contentType(MediaType.APPLICATION_STREAM_JSON).body(p))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}

	@PostMapping
	public Mono<ResponseEntity<Curso>> registrar(@Valid @RequestBody Curso plato, final ServerHttpRequest req) {
		return service.registrar(plato)
				.map(p -> ResponseEntity.created(URI.create(req.getURI().toString().concat("/").concat(p.getId())))
						.contentType(MediaType.APPLICATION_STREAM_JSON).body(p))
				.onErrorMap(error -> new ArithmeticException("Ups"));
	}

	@PutMapping
	public Mono<ResponseEntity<Curso>> modificar(@Valid @RequestBody Curso plato) {
		return service.modificar(plato)
				.map(p -> ResponseEntity.ok().contentType(MediaType.APPLICATION_STREAM_JSON).body(p))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}

	@DeleteMapping("/{id}")
	public Mono<ResponseEntity<Void>> eliminar(@PathVariable("id") String id) {
		return service.listarPorId(id).flatMap(p -> {
			return service.eliminar(p.getId()).then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
		}).defaultIfEmpty(ResponseEntity.notFound().build());
	}

}
