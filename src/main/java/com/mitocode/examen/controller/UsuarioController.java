package com.mitocode.examen.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.examen.document.Usuario;
import com.mitocode.examen.service.IUsuarioService;

import reactor.core.publisher.Mono;

/**
 * @author jcasco
 */
@RestController
@RequestMapping("/usuario")
public class UsuarioController {

	@Autowired
	private IUsuarioService service;

	@Autowired
	BCryptPasswordEncoder passwordEncoder;
	
	@PostMapping
	public Mono<ResponseEntity<Usuario>> registrar(@Valid @RequestBody Usuario usuario, final ServerHttpRequest req) {
		usuario.setClave(passwordEncoder.encode(usuario.getClave()));
		return service.registrar(usuario)
				.map(p -> ResponseEntity.created(URI.create(req.getURI().toString().concat("/").concat(p.getId())))
						.contentType(MediaType.APPLICATION_STREAM_JSON).body(p))
				.onErrorMap(error -> new ArithmeticException("Ups"));
	}

}
